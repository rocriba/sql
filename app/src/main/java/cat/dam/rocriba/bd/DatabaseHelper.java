package cat.dam.rocriba.bd;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static android.content.Context.MODE_PRIVATE;

public class DatabaseHelper extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;
    private final Context context;
     public static SQLiteDatabase db;

  //  @SuppressLint("SdCardPath")
    private static final String DATABASE_PATH = "/data/data/com.dam.rocriba.bd/assets/";
    private static final String DATABASE_NAME = "usuaris.db";
    private final String USER_TABLE = "user";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
       // DATABASE_PATH = context.getDatabasePath(DATABASE_NAME).getPath();
        createDb();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        /*
        final String TAULA_USUARIS = "usuaris";
        final String CLAU_ID = "id";
        final String CLAU_USUARI = "usuari";
        final String CLAU_CONTRASENYA = "contrasenya";
        final String CREA_TAULA_USUARIS = "CREATE TABLE "
                + TAULA_USUARIS + "(" + CLAU_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT," + CLAU_USUARI + " TEXT" +CLAU_CONTRASENYA +"TEXT );";
        db.execSQL(CREA_TAULA_USUARIS);

        db.execSQL("INSERT INTO usuaris VALUES (1,'admin','admin');");
         String path = DATABASE_PATH + DATABASE_NAME;
        SQLiteDatabase pp = openOrCreateDatabase(File usuaris.db, "users",MODE_PRIVATE,null);
        db.execSQL ("CREATE TABLE IF NOT EXISTS users(username TEXT, password TEXT);");
        db.execSQL ("INSERT INTO users VALUES ('admin', 'admin');");

         */


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void createDb(){

        boolean dbExist = checkDbExist();

        if(!dbExist){
            this.getReadableDatabase();
            copyDatabase();
        }
    }

    private boolean checkDbExist(){
        SQLiteDatabase sqLiteDatabase = null;

        try{
            String path = DATABASE_PATH + DATABASE_NAME;
            sqLiteDatabase = SQLiteDatabase.openDatabase(path, null, 0);
        } catch (Exception ex){
        }

        if(sqLiteDatabase != null){
            sqLiteDatabase.close();
            return true;
        }

        return false;
    }

    private void copyDatabase(){
        try {

            InputStream inputStream = context.getAssets().open(DATABASE_NAME);

            String outFileName = DATABASE_PATH + DATABASE_NAME;

            OutputStream outputStream = new FileOutputStream(outFileName);

            byte[] b = new byte[1024];
            int length;

            while ((length = inputStream.read(b)) > 0){
                outputStream.write(b, 0, length);
            }

            outputStream.flush();
            outputStream.close();
            inputStream.close();



        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    private SQLiteDatabase openDatabase(){
        String path = DATABASE_PATH + DATABASE_NAME;
        db = SQLiteDatabase.openDatabase( path, null, SQLiteDatabase.OPEN_READWRITE);
        return db;
    }

    public void close(){
        if(db != null){
            db.close();
        }
    }

    public boolean checkUserExist(String username, String password){
        String[] columns = {"username"};
        db = openDatabase();

        String selection = "username=? and password = ?";
        String[] selectionArgs = {username, password};

        Cursor cursor = db.query(USER_TABLE, columns, selection, selectionArgs, null, null, null);
        int count = cursor.getCount();

        cursor.close();
        close();

        if(count > 0){
            return true;
        } else {
            return false;
        }
    }}

    //ERROR: https://forums.bignerdranch.com/t/error-connecting-to-sqlite-db/17150

//DB: https://www.youtube.com/watch?v=312RhjfetP8