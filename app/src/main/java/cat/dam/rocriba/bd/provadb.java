package cat.dam.rocriba.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class provadb extends SQLiteOpenHelper {
    public static String NOM_BD = "bd_usuaris";
    private static final int VERSIO_BD = 1;
    private static final String TAULA_USUARIS = "usuaris";
    private static final String CLAU_ID = "id";
    private static final String CLAU_USUARI = "usuari";
    private static final String CLAU_CONTRASENYA = "contrasenya";
    // Utilitza un String parametritzat per a la instrucció que crea de la taula alumnes
    private static final String CREA_TAULA_USUARIS = "CREATE TABLE "
            + TAULA_USUARIS + "(" + CLAU_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + CLAU_USUARI + " TEXT" + CLAU_CONTRASENYA + "TEXT );";
    public provadb(Context context) {
        super(context, NOM_BD, null, VERSIO_BD);
        Log.d("table", CREA_TAULA_USUARIS);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        //quan es crea l'objecte a partir de la classe crearà la taula si no està creada
        db.execSQL(CREA_TAULA_USUARIS);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int versio_antiga, int versio_nova) {
        db.execSQL("DROP TABLE IF EXISTS '" + TAULA_USUARIS + "'");
        onCreate(db);
    }
    public long afegeix_detallAlumne(String alumne) {
        SQLiteDatabase db = this.getWritableDatabase();
        // Crea valors de contingut
        ContentValues valors = new ContentValues();
        valors.put(CLAU_USUARI, alumne);
        // inserta una fila a la taula alumnes
        long insert = db.insert(TAULA_USUARIS, null, valors);
        return insert;
    }
    public ArrayList<String> obte_llistaAlumnes() {
        ArrayList<String> arrayList_Alumnes = new ArrayList<String>();
        String nom="";
        String selectQuery = "SELECT * FROM " + TAULA_USUARIS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        // Recorre totes les files i les afegeix a la llista
        if (c.moveToFirst()) {
            do {
                nom = c.getString(c.getColumnIndex(CLAU_USUARI));
                // afegeix a la llista d'alumnes
                arrayList_Alumnes.add(nom);
            } while (c.moveToNext());
            Log.d("array", arrayList_Alumnes.toString());
        }
        return arrayList_Alumnes;
    }
}
